import 'package:flutter/material.dart';
import 'package:murojaah/src/screen/Home/home.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with SingleTickerProviderStateMixin{
 
  TabController tabController;
  ScrollController scrollViewController;
  int _currentIndex = 0;
  final List<Widget> _children = [
     new Home(),
            new Home(),
            new Home(),
            new Home(),
  ];

  @override
  void initState() {
    super.initState();
    tabController = new TabController(vsync: this, length: 4);
    scrollViewController = new ScrollController();
  }

  @override
  void dispose() {
    tabController.dispose();
    scrollViewController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    Widget searchBar = new Container(
    height: 44.0,
    margin: EdgeInsets.only(top: 8.0),
    width: MediaQuery.of(context).size.width,
    padding: EdgeInsets.symmetric(horizontal: 8.0),
    color: Colors.red[800],
      child :new Material(
            elevation: 2,
            borderRadius: new BorderRadius.circular(3.5),
            child: new Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: new Container(
                        padding: EdgeInsets.only(left: 30.0),
                        child : new TextField(
                          decoration: InputDecoration.collapsed(
                            hintText: "UMKM",
                            hintStyle: TextStyle(
                              fontSize: 19.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.red[800],
                              fontFamily: 'NotoSans',
                              letterSpacing: 0.9
                            ),
                          ),
                          obscureText: false,
                          onSubmitted: (String place){
                              print(place);
                          },
                        ),
                      )
                    ),
                    new Padding(
              padding:EdgeInsets.all(5.0),
              child: IconButton(
              icon: new Icon(
              Icons.search,
              color: Colors.red[800],
              size: 24.0,
            ),
            onPressed: (){
              print("your notify action here");
            })
           )
                  
                  ],
            ),

          ),
    );
    return new Scaffold(
      // drawer: LeftDrawer(),
      body: new NestedScrollView(
        controller: scrollViewController,
        headerSliverBuilder: (BuildContext context,bool boxIsScrolled){
        return <Widget>[
          SliverAppBar(
            title: searchBar,
            titleSpacing: 0,
            backgroundColor: Colors.red[800],
            automaticallyImplyLeading: false,
            pinned: true,
            floating: true,
            forceElevated: boxIsScrolled,         
          ),
          
        ];
      },
    body: _children[_currentIndex],
    ),
    bottomNavigationBar: BottomNavigationBar(
       onTap: onTabTapped, // new
              currentIndex: _currentIndex, 
              items: [
                BottomNavigationBarItem(
                  icon: new Icon(Icons.home),
                  title: new Text('Home'),
                ),
                BottomNavigationBarItem(
                  icon: new Icon(Icons.mail),
                  title: new Text('Messages'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                  title: Text('Profile')
                )
              ],
            ),
         );
         }
       
  void onTabTapped(int index) {
   setState(() {
     _currentIndex = index;
   });
 }
}

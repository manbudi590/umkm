import 'package:flutter/material.dart';


class DetailJus extends StatefulWidget {
  String jus1;
  @override
  _DetailJusState createState() => _DetailJusState();

  DetailJus({@required this.jus1})
      : assert(jus1 != null);

}

class _DetailJusState extends State<DetailJus> {
  
  Widget synopsis = new  SizedBox(
    width: 650,
        child:Card(
          color: Colors.white,
          elevation: 6.0,
          child: new Wrap(
            children: <Widget>[
              Text("وَقَالَ رَجُلٌ مُؤْمِنٌ مِنْ آلِ فِرْعَوْنَ يَكْتُمُ إِيمَانَهُ أَتَقْتُلُونَ رَجُلًا أَنْ يَقُولَ رَبِّيَ اللَّهُ وَقَدْ جَاءَكُمْ بِالْبَيِّنَاتِ مِنْ رَبِّكُمْ ۖ وَإِنْ يَكُ كَاذِبًا فَعَلَيْهِ كَذِبُهُ ۖ وَإِنْ يَكُ صَادِقًا يُصِبْكُمْ بَعْضُ الَّذِي يَعِدُكُمْ ۖ إِنَّ اللَّهَ لَا يَهْدِي مَنْ هُوَ مُسْرِفٌ كَذَّابٌ",style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),textAlign: TextAlign.right,),
              Padding(padding: EdgeInsets.only(left: 120.0)),
            ],
          ),
        ),
  );

  Widget play = new Container(
    height:150.0,
      child:  new InkWell(
      onTap:(){
          print('more recomended');
      },
        child:Icon(Icons.play_arrow,color: Colors.black,size: 125),
      )
  );
  Widget star = Text("Mulai",
    style: TextStyle(
      shadows: <Shadow>[
              Shadow(
                offset: Offset(0.4, 1.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 0, 0, 0),
              ),
            ],
      color: Colors.black,
      fontSize: 25.0,
      fontFamily: 'Poppins',
      fontWeight: FontWeight.bold,
    )
  );
  


  @override
  Widget build(BuildContext context) {
        return Scaffold(
          body: new NestedScrollView(
              
              headerSliverBuilder: (BuildContext context,bool boxIsScrolled){
                return <Widget>[                 
                  SliverAppBar(
                    elevation: 0,
                    backgroundColor: Colors.brown,
                     expandedHeight: 157.0,
                     titleSpacing: 0,
                     flexibleSpace: new FlexibleSpaceBar(
                       centerTitle: true,
                       title: new Wrap(
                          direction: Axis.vertical,
                          children: <Widget>[
                            new Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              new Center(
                                child:new Align(
                                    alignment: Alignment.bottomCenter,
                                    child: new Text("Surat Al Fatihah",
                                            style: new TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.white,
                                            shadows: [
                                                Shadow(
                                                  blurRadius: 0.4,
                                                  color: Colors.black,
                                                  offset: Offset(1.0, 1.0),
                                                ),
                                              ],
                                            ),
                                          ),
                                      ),
                                ),
                                new Center(
                                  child:new Align(
                                    alignment: Alignment.bottomCenter,
                                    child: new Text("Ayat 1",
                                            style: new TextStyle(
                                            fontSize: 13.0,
                                            color: Colors.white,
                                            shadows: [
                                                Shadow(
                                                  blurRadius: 0.4,
                                                  color: Colors.black,
                                                  offset: Offset(1.0, 1.0),
                                                ),
                                              ],
                                            ),
                                          ),
                                      ),
                                )
                              ]
                            )
                          ]
                       ),
                       background: new Image.network(
                         "http://4.bp.blogspot.com/-bRPGKX0VxJA/V-onTEWjJWI/AAAAAAAAAG0/fUT5ms8GPGMI9neqZ-ZSAQOmIpBcXpQMACK4B/s1600/al-quran-1366x768.jpg",
                         fit: BoxFit.cover,
                       ),
                     ),
                     automaticallyImplyLeading: true,
                     pinned: true,
                     floating: false,
                     forceElevated: boxIsScrolled,
                  ),
                ];
              },
              body: new ListView(
                    padding: EdgeInsets.all(0),
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                            new Material(
                              borderRadius: BorderRadius.circular(4.0),
                              elevation:0.0,
                              child: new Container(
                                color: Colors.white,
                                 padding: EdgeInsets.all(8.0),
                                 child: new Column( 
                                   children: <Widget>[
                                     new Container(
                                       padding: EdgeInsets.only(top: 8.0,bottom: 15.0),
                                        decoration: new BoxDecoration(
                                          border: Border(
                                            bottom: BorderSide(
                                              color: Colors.grey[200]
                                            )
                                          ),
                                        ),
                                        child: new Align(
                                          alignment: Alignment.centerLeft,
                                          child: new Row(
                                            children: <Widget>[
                                                new Padding(
                                                  padding: EdgeInsets.only(left: 10),
                                                  child: new Text(
                                                  'Ayat :',
                                                  style: TextStyle(
                                                    fontSize: 16.0,
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w700
                                                  ),
                                                ),
                                                ),
                                                Spacer(),
                                                
                                                
                                            ],
                                          )
                                        ),
                                     ),
                                     synopsis,
                                     play,
                                     star
                                   ],
                                 ),
                               ),
                            )
                         ],
                   
            ),

          ),

        );
  }
}



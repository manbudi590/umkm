import 'package:flutter/material.dart';
import 'package:murojaah/src/screen/Play/detail_jus.dart';
// import 'package:murojaah/src/screen/Play/detail_jus.dart';

class Hal_Setor extends StatefulWidget {
  @override
  _Hal_SetorState createState() => _Hal_SetorState();
}

class _Hal_SetorState extends State<Hal_Setor> {
  
  List<List<String>> jus;
  
  @override
  void initState() {
    jus = [
      ['Jus 1'],['Jus 2'],['Jus 3'],['Jus 4'],['Jus 5'],['Jus 6'],['Jus 7'],['Jus 8'],['Jus 9'],['Jus 10'],
      ['Jus 11'],['Jus 12'],['Jus 13'],['Jus 14'],['Jus 15'],['Jus 16'],['Jus 17'],['Jus 18'],['Jus 19'],['Jus 20'],
      ['Jus 21'],['Jus 22'],['Jus 23'],['Jus 24'],['Jus 25'],['Jus 26'],['Jus 27'],['Jus 28'],['Jus 29'],['Jus 30'],
    ];
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text("Murojaah",
                      style: TextStyle(
                        shadows: <Shadow>[
                                Shadow(
                                  offset: Offset(0.4, 1.0),
                                  blurRadius: 3.0,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                              ],
                        color: Colors.white,
                        fontSize: 25.0,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                      )),
                  background: Image.network(
                    "http://4.bp.blogspot.com/-bRPGKX0VxJA/V-onTEWjJWI/AAAAAAAAAG0/fUT5ms8GPGMI9neqZ-ZSAQOmIpBcXpQMACK4B/s1600/al-quran-1366x768.jpg",
                    fit: BoxFit.cover,
                  )),
            ),
          ];
        },
      body:  Container(
        padding: EdgeInsets.only(top: 30.0),
        child: GridView.count(
        crossAxisCount: 1,
        padding: EdgeInsets.all(16.0),
        childAspectRatio: 2.2,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
        children: <Widget>[

         for(List l in jus)
         GestureDetector(
          child :myGridItems(l[0], "https://i.pinimg.com/originals/7b/4c/3e/7b4c3e225fe6fc74256f5c1d0608668b.jpg", 0xFFEF9A9A, 0xFFE57373),
         onTap: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailJus(jus1:l[0])));
          },
         ),
        ],
      ),

          ),

        ), 
    );
  }

Widget myGridItems(String gridName, String gridimage, int color, int color1){
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(24.0),
      gradient: new LinearGradient(
        colors: [
          Color(color),

          Color(color1),
        ],
        begin: Alignment.centerLeft,
        end: new Alignment(1.0,1.0), 
      ),
    ),

    child: Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.3,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              image: DecorationImage(
                image: new NetworkImage(gridimage),
                fit: BoxFit.fill,
              )
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 10.0,),
                  Container(child: Icon(Icons.book),),
                  SizedBox(width: 10.0,),
                  Container(child: Text("Murojaah", style: TextStyle(color: Colors.white,fontSize: 16.0),),),
                  SizedBox(width: 10.0,),
                 
                                  ],),),
                  Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(gridName,style: TextStyle(color:Colors.white,fontSize: 20.0, fontWeight: FontWeight.bold)),
                  ),
          ],
        ),
      ],
    ),
  );
}
}

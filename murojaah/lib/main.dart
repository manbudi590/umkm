
import 'package:flutter/material.dart';
import 'package:murojaah/src/screen/Home/home.dart';
import 'package:murojaah/src/screen/Setor/hal_setor.dart';


void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: '/',
      routes: <String, WidgetBuilder>{
        '/': (context) => Home(),
        '/halsetor': (context) => Hal_Setor(),
      },
  ));
}

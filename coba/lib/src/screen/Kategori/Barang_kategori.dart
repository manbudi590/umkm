import 'package:coba/src/widgets/barang_item_card.dart';

import 'package:flutter/material.dart';
import 'package:flutter_launch/flutter_launch.dart';


class BarangKategoriScreen extends StatefulWidget{
  @override
  String namaK;
  _BarangKategoriScreenState createState() => _BarangKategoriScreenState();
  BarangKategoriScreen({@required this.namaK})
      : assert(namaK != null);
}

class _BarangKategoriScreenState extends State<BarangKategoriScreen>{
  List<List<String>> imagesPath;
  List<List<String>> imagesK;
  String jenis;
  @override
  void initState() {
    widget.namaK=="Makanan"?imagesPath = [
      ['https://cdn.moneysmart.id/wp-content/uploads/2018/12/08181221/Makanan-ini-paling-pas-disantap-saat-musim-hujan-turun-700x497.jpg','Chiken Diner','1000',
      'https://awsimages.detik.net.id/customthumb/2011/11/29/363/mikuahmisokcl.jpg?w=700&q=90',
      'http://images.detik.com/content/2010/01/19/363/mikuahmisobsr.jpg'
      ],
      ['https://iradiofm.com/wp-content/uploads/2017/02/9c85e091350f418eb4ee4a3ad1dd3e86-1.jpg','Mie Telur','2000',
      'https://citraindonesia.com/wp-content/uploads/2018/07/resep-mie-goreng-telur-spesial.jpg',
      'https://live.staticflickr.com/2414/2482273431_221cefc7d2.jpg'      
      ],
      ['https://cdn-u1-gnfi.imgix.net/post/large-543defb8b5718-86712ff11c99c09cb60453869a030596.jpg','Sate','40000',
      'https://awsimages.detik.net.id/community/media/visual/2018/08/13/85bb9003-9ef8-45c2-ad67-d62a17d7e4c7.jpeg?w=700&q=90',
      'http://4.bp.blogspot.com/-sZXZJziPtmc/UgzCMtqko1I/AAAAAAAAMKs/XjNvUL1pzfg/s400/Sate+Kambing+Bumbu+Kecap.jpg'
      ],
      ['https://hellosehat.com/wp-content/uploads/2018/09/makanan-kekinian-tinggi-kalori.jpg','Martabak','50000',
      'https://user-uploaded-asset.s3.amazonaws.com/uploads/bootsy/image/1946/medium_martabak_asin.jpg',
      'https://awsimages.detik.net.id/customthumb/2016/01/18/289/173105_martabakborneocover.jpg?w=700&q=90'
      ],
      ['https://hellosehat.com/wp-content/uploads/2018/09/makanan-kekinian-tinggi-kalori.jpg','Martabak','50000',
      'https://user-uploaded-asset.s3.amazonaws.com/uploads/bootsy/image/1946/medium_martabak_asin.jpg',
      'https://awsimages.detik.net.id/customthumb/2016/01/18/289/173105_martabakborneocover.jpg?w=700&q=90'
      ],
    ]:imagesPath = [
      ['https://lombokigh.files.wordpress.com/2013/03/desa-sade-oleh-oleh.jpg','Piring','1000'],
      ['http://www.pergiberwisata.com/wp-content/uploads/2016/09/Tembikar-Lombok.jpg','Guci','2000'],
      ['https://mobillombok.com/wp-content/uploads/2018/01/Kain-Tenun-Khas-Sasak-Lombok-Cocok-untuk-Oleh-Oleh-dari-Lombok-Nusa-Tenggara-Barat.jpg','Kain Tenun','40000'],
      ['https://2.bp.blogspot.com/-7LETnPpTs-w/U1TaSbgifRI/AAAAAAAABE8/z2_7hlypl2M/s1600/Jual+Tas+cantik+dari+Ketak+Lombok.jpg','Tas','50000'],
    ];
    

    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    
    
    final appBar = AppBar(
      elevation: 5,
      title: Text("UMKM",textAlign: TextAlign.center),
      backgroundColor: Colors.red[800],
    );
    final imageSection= 
          Image.asset('assets/images/makananfoto.png');
    final imageSection1= 
          Image.asset('assets/images/kerajinanfoto.png');


    final grid = Container(  
      child :GridView.count(
            crossAxisCount: 2,
            children: <Widget>[
              Container(
                color: Colors.yellowAccent,
                height: double.infinity, // tambahkan property berikut
                child: Center(
                  child: Text("1", style: TextStyle(fontSize: 24.0),),
                ),
              ),
              Container(
                color: Colors.blueAccent,
                height: double.infinity, // tambahkan property berikut
                child: Center(
                  child: Text("2", style: TextStyle(fontSize: 24.0),),
                ),
              ),
              Container(
                color: Colors.brown,
                height: double.infinity, // tambahkan property berikut
                child: Center(
                  child: Text("3", style: TextStyle(fontSize: 24.0),),
                ),
              ),
              Container(
                color: Colors.orange,
                height: double.infinity, // tambahkan property berikut
                child: Center(
                  child: Text("4", style: TextStyle(fontSize: 24.0),),
                ),
              ),
            ],
            ),
          );

    return Scaffold(
      appBar: appBar,
      body:  
      CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              [
                widget.namaK=="Makanan"?
                imageSection
                :
                imageSection1
              ]
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Card(
                  child:
                  Text(widget.namaK,textAlign: TextAlign.center,style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold))
                ),
                
              ]
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 0.7,

              
            ),
            delegate: SliverChildListDelegate(
              [
                for(List l in imagesPath)
                  BarangItemCard(imagesPath:l[0],nama:l[1],harga:l[2]),
              ]
            ),
          )
        ],      
      ),
    );
  }
}


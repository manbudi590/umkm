import 'package:flutter/material.dart';
import 'package:flutter_launch/flutter_launch.dart';



const styleHeadline = TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30);
const styleTitle = TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18);
const styleSubtitle = TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12);

class DetailBarangScreen extends StatefulWidget{
  String imagesPath,nama,harga;
  @override
  _DetailBarangScreenState createState() => _DetailBarangScreenState();
  DetailBarangScreen({@required this.imagesPath,@required this.nama,@required this.harga});

  

}

class _DetailBarangScreenState extends State<DetailBarangScreen>{
  




  // popupDialog(){
  //   return showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         content: Form(
  //           child: Container(
  //             decoration: BoxDecoration(
  //               borderRadius: BorderRadius.all(Radius.circular(15))
  //             ),
  //             child: Column(
  //               mainAxisSize: MainAxisSize.min,
  //               children: <Widget>[
  //                 Padding(
  //                   padding: EdgeInsets.all(8.0),
  //                   child: Text('Entry Price Suggestion', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
  //                 ),
  //                 Padding(
  //                   padding: EdgeInsets.all(8.0),
  //                   child: TextField(

  //                   ),
  //                 ),
  //                 Padding(
  //                   padding: EdgeInsets.all(8.0),
  //                   child: TextField(),
  //                 ),
  //                 Padding(
  //                   padding: const EdgeInsets.all(8.0),
  //                   child: RaisedButton(
  //                     child: Text("Submit"),
  //                     onPressed: () => print('submit price'),
  //                   ),
  //                 )
  //               ],
  //             ),
  //           ),
  //         ),
  //       );
  //     });
  // }
  void whatsAppOpen() async {
    await FlutterLaunch.launchWathsApp(phone: "+621909000103", message: "Hello");
  }

  @override
  Widget build(BuildContext context) {
      final appBar = AppBar(
      title: Text("UMKM",textAlign: TextAlign.center,),
      elevation: 5,
      backgroundColor: Colors.red[800],
    );

      final topLeft = Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Hero(
            tag: "Komputer",
            child: Material(
              elevation: 15.0,
              shadowColor: Colors.purple.shade900,
              child: Image.network(
              widget.imagesPath,
                width: 250,
                height: 225,
                fit: BoxFit.cover,
            ),
            ),
          ),
        ),
       
      ],
    );


    ///detail top right
    final topRight = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        text(widget.nama,
            size: 26, isBold: true,color: Colors.white70, padding: EdgeInsets.only(top: 26.0)),
        text(
          'oleh budi',
          color: Colors.white30,
          size: 12,
          padding: EdgeInsets.only(top: 8.0, bottom: 16.0),
        ),
        Row(
          children: <Widget>[
            text(
              widget.harga,
              isBold: true,
              color: Colors.white70,
              padding: EdgeInsets.only(right: 8.0),
            ),
            
           
          ],
        ),
        SizedBox(height: 32.0),
        Material(
          borderRadius: BorderRadius.circular(20.0),
          shadowColor: Colors.blue.shade200,
          elevation: 5.0,
          
          child: MaterialButton(
            
            onPressed: () {
              whatsAppOpen();
            },
            minWidth: 160.0,
            color: Colors.green,
             child: Row(
               
               mainAxisAlignment: MainAxisAlignment.center,
               crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  
                text('Whatsup', color: Colors.white, size: 13),
                new Icon(
                  Icons.phone,
                  color: Colors.white,
                  size: 24.0,
                ),
                
                ]
             ) 
             


          ),
          
        ),
        
      ],
    );

    final topContent = Container(
      color: Colors.red[800],
      padding: EdgeInsets.only(bottom: 16.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(flex: 3, child: topLeft),
          Flexible(flex: 3, child: topRight),
        ],
      ),
    );

    

    return Scaffold(
      appBar: appBar,
      
      body: SingleChildScrollView(
       child:Column(
        children: <Widget>[topContent, new ReviewWidget(), new ReviewWidget()],
      ),
      )
    );
  }
  ///create text widget
  text(String data,
          {Color color = Colors.black87,
          num size = 14,
          EdgeInsetsGeometry padding = EdgeInsets.zero,
          bool isBold = false}) =>
      Padding(
        padding: padding,
        child: Text(
          data,
          style: TextStyle(
              color: color,
              fontSize: size.toDouble(),
              fontWeight: isBold ? FontWeight.bold : FontWeight.normal),
        ),
      );
}

class ReviewWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child:Column(
        children: <Widget>[
          
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Text('Best demonstration of this case is Google Allo AppBar.Ive no idea if the AppBar shadow is animated or only the background color.If shadow is animated then its linked to the color'),
          ),
          SizedBox(
            height: 20,
            child: Divider(),
          )
        ]
      ),
    );
  }
}


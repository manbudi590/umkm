import 'package:coba/src/app.dart';

import 'package:coba/src/widgets/backgroundcurve.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  @override
  Widget build(BuildContext context) {
    final email =TextField(
      obscureText: false,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          filled: true,
          fillColor: Colors.white,
          
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
              ),
    );
    final passwordField = 
    TextField(
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          filled: true,
          fillColor: Colors.white,
          border: 
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
             
    );
    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            minWidth: MediaQuery.of(context).size.width,
            onPressed: () {
              Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => App()),
            );

            },
            child: Text("Login",
                textAlign: TextAlign.center,
                style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
          ),
        );
    return Scaffold(
      
      body: SingleChildScrollView(
      child :new Stack(
        children: <Widget>[
          BackgroundCurve(),
           Padding(
                padding: const EdgeInsets.all(20.0),
          child :Column(
            crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 100.0),
                    SizedBox(
                      height: 100.0,
                      child: Image.asset(
                        "assets/images/log.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    
                    email,
                    SizedBox(height: 25.0),
                    passwordField,
                    SizedBox(
                      height: 35.0,
                    ),
                    loginButon,
                    SizedBox(
                      height: 3.0,
                    ),
                  ],
          ),
        ),
        ],
      ),
      ),
    );
  }
}
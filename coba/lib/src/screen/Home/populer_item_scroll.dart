import 'package:flutter/material.dart';
import 'package:coba/src/widgets/populer_item_card.dart';

class PopulerItemScroll extends StatelessWidget {
  final List<List<String>> village;

  PopulerItemScroll({
    @required this.village,
  }) : assert(village != null);

  @override
  Widget build(BuildContext context) {
    List<Widget> villageList = List<Widget>();
    for (List l in village) {
      villageList.add(PopulerItemCard(name: l[0], image: l[1]));
    }

    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.fromLTRB(8, 12, 6, 12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Pelatihan Terbaru',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                 Row(
                    children: <Widget>[
                      Text('lainnya'),
                      Icon(Icons.arrow_right,
                        color: Colors.black,
                        size: 10.0,),
                    ],
                  ),
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
                height: 280,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: villageList.toList(),
                )),
          ],
        ),
      ),
    );
  }
}

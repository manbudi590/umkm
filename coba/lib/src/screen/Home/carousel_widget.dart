import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

class CarouselWidget extends StatefulWidget{
  @override
  _CarouselWidgetState createState() => _CarouselWidgetState();
}

class _CarouselWidgetState extends State<CarouselWidget>{

  List<String> imagesPath;

  @override
  void initState() {
    imagesPath = [
      'https://cdns.klimg.com/merdeka.com/i/w/news/2018/05/15/976670/670x335/gandeng-jne-bukalapak-gratiskan-ongkos-kirim-selama-ramadan.jpg',
      'https://pbs.twimg.com/media/B6j3ANSCQAAH0Qt.png',
      'https://pbs.twimg.com/media/B6j3ANSCQAAH0Qt.png',
      'https://pbs.twimg.com/media/B6j3ANSCQAAH0Qt.png',
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List imageList = new List();
    for(String path in imagesPath){
      imageList.add(
          NetworkImage(path)
      );
    }

    return Container(
      child: SizedBox(
        height: 200.0,
        width: 350.0,
        child: new Carousel(
          images: imageList,
          dotSize: 4.0,
          dotSpacing: 15.0,
          dotColor: Colors.lightGreenAccent,
          indicatorBgPadding: 5.0,
        )
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:coba/src/widgets/barang_item_card.dart';

class BarangItemScroll extends StatelessWidget{
  List<List<String>> commodity;
  List<List<String>> imagesPath;
  String kode;

  BarangItemScroll({
    @required this.imagesPath, @required this.kode
  }):   assert(imagesPath != null);

  @override
  Widget build(BuildContext context) {
    final tmakan= Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Makanan Baru di Tambahkan', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  Row(
                    children: <Widget>[
                      Text('lainnya'),
                      Icon(Icons.arrow_right,
                        color: Colors.black,
                        size: 10.0,),
                    ],
                  ),
                  
                ],
              );
    final tkerajinan= Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Kerajinan Baru di Tambahkan', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  Row(
                    children: <Widget>[
                      Text('lainnya'),
                      Icon(Icons.arrow_right,
                        color: Colors.black,
                        size: 10.0,),
                    ],
                  ),
                ],
              );

    List <Widget> commodityList = List<Widget>();
    for(List l in imagesPath){
      commodityList.add(
          // BarangItemCard(onPressed: () => print('item card'))
          BarangItemCard(imagesPath:l[0],nama:l[1],harga:l[2])
      );
    }
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.fromLTRB(8, 12, 6, 12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(10,10,10,4),
              child:
              kode=="Makanan"?
                tmakan
                :
                tkerajinan
            ),
            Container(
                padding: EdgeInsets.fromLTRB(10,4,10,4),
                height: 280,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children:  commodityList.toList(),
                )
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:coba/src/screen/Home/carousel_widget.dart';
import 'package:coba/src/screen/Home/barang_item_scroll.dart';
import 'package:coba/src/screen/Home/barang_category_widget.dart';
import 'package:coba/src/screen/Home/populer_item_scroll.dart';



class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<List<String>> category;
  List<List<String>> village;
  List<List<String>> commodity;
  List<List<String>> imagesPath;
  List<List<String>> imagesK;

  @override
  void initState() {
    category = [
      ['Makanan', 'assets/images/makanan.png'],
      ['Pelatihan', 'assets/images/learning.png'],
      ['Kerajinan', 'assets/images/crafts.png'],
      ['Jasa', 'assets/images/team.png']
    ];
    village = [
      ['Kekeri', 'https://www.rumahcoding.co.id/wp-content/uploads/2019/04/intermediate.jpeg'],
      ['Kota Mataram', 'https://anj-group.com/backend/elfinder/files/pelibatan%20masyarakat/mama-papua-bertani-sayuran2.jpg']
    ];
    
    imagesPath = [
      ['https://cdn.moneysmart.id/wp-content/uploads/2018/12/08181221/Makanan-ini-paling-pas-disantap-saat-musim-hujan-turun-700x497.jpg','Chiken Diner','1000',
      'https://awsimages.detik.net.id/customthumb/2011/11/29/363/mikuahmisokcl.jpg?w=700&q=90',
      'http://images.detik.com/content/2010/01/19/363/mikuahmisobsr.jpg'
      ],
      ['https://iradiofm.com/wp-content/uploads/2017/02/9c85e091350f418eb4ee4a3ad1dd3e86-1.jpg','Mie Telur','2000',
      'https://citraindonesia.com/wp-content/uploads/2018/07/resep-mie-goreng-telur-spesial.jpg',
      'https://live.staticflickr.com/2414/2482273431_221cefc7d2.jpg'      
      ],
      ['https://cdn-u1-gnfi.imgix.net/post/large-543defb8b5718-86712ff11c99c09cb60453869a030596.jpg','Sate','40000',
      'https://awsimages.detik.net.id/community/media/visual/2018/08/13/85bb9003-9ef8-45c2-ad67-d62a17d7e4c7.jpeg?w=700&q=90',
      'http://4.bp.blogspot.com/-sZXZJziPtmc/UgzCMtqko1I/AAAAAAAAMKs/XjNvUL1pzfg/s400/Sate+Kambing+Bumbu+Kecap.jpg'
      ],
      ['https://hellosehat.com/wp-content/uploads/2018/09/makanan-kekinian-tinggi-kalori.jpg','Martabak','50000',
      'https://user-uploaded-asset.s3.amazonaws.com/uploads/bootsy/image/1946/medium_martabak_asin.jpg',
      'https://awsimages.detik.net.id/customthumb/2016/01/18/289/173105_martabakborneocover.jpg?w=700&q=90'
      ],
    ];

    imagesK = [
      ['https://lombokigh.files.wordpress.com/2013/03/desa-sade-oleh-oleh.jpg','Piring','1000'],
      ['http://www.pergiberwisata.com/wp-content/uploads/2016/09/Tembikar-Lombok.jpg','Guci','2000'],
      ['https://mobillombok.com/wp-content/uploads/2018/01/Kain-Tenun-Khas-Sasak-Lombok-Cocok-untuk-Oleh-Oleh-dari-Lombok-Nusa-Tenggara-Barat.jpg','Kain Tenun','40000'],
      ['https://2.bp.blogspot.com/-7LETnPpTs-w/U1TaSbgifRI/AAAAAAAABE8/z2_7hlypl2M/s1600/Jual+Tas+cantik+dari+Ketak+Lombok.jpg','Tas','50000'],
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: LeftDrawer(),
      body: Container(
        color: Colors.white,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.all(0),
                child: CarouselWidget(),
              ),
            ),
            BarangCategoryWidget(category: category),
            BarangItemScroll(imagesPath:imagesPath,kode:"Makanan"),
            BarangItemScroll(imagesPath:imagesK,kode:"Kerajinan"),
            
            PopulerItemScroll(village: village),
          ],
        ),
      ),
    );
  }
}

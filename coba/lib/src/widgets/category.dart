
import 'package:coba/src/screen/Kategori/Barang_kategori.dart';
import 'package:flutter/material.dart';

class CategoryCard extends StatelessWidget {
  String categoryImage;
  String categoryText;

  CategoryCard({@required this.categoryImage, @required this.categoryText})
      : assert(categoryImage != null),
        assert(categoryText != null);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          child: new InkWell(
            onTap: () {
              if(categoryText=='Makanan'){
               Navigator.push(context,
                MaterialPageRoute(builder: (context) => BarangKategoriScreen(namaK:"Makanan")
              ));
              }else if(categoryText=='Pelatihan'){
              //  Navigator.push(context,
              //   MaterialPageRoute(builder: (context) => MakananKategoriScreen()
              // ));
              }else if(categoryText=='Kerajinan'){
               Navigator.push(context,
                MaterialPageRoute(builder: (context) => BarangKategoriScreen(namaK:"Kerajinan")
              ));
              }else if(categoryText=='Jasa'){
              //  Navigator.push(context,
              //   MaterialPageRoute(builder: (context) => MakananKategoriScreen()
              // ));
              }
            },
            child: Image.asset(
              categoryImage,
              width: 50,
              height: 50,
            ),
          ),
        ),
        Text(categoryText)
      ],
    );
  }
}

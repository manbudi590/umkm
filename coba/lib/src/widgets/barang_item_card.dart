import 'package:coba/src/screen/Detail/detail_barang_screen.dart';
import 'package:flutter/material.dart';

class BarangItemCard extends StatelessWidget {
  final double _borderRadius = 10;
  List<List<String>> name;
  String imagesPath,nama,harga;
  // final Function onPressed;

  BarangItemCard({@required this.imagesPath,@required this.nama,@required this.harga});

  @override
  Widget build(BuildContext context) {
    // List imageList = new List();
    // for(String path in imagesPath){
    //   imageList.add(
    //      Image.network(path)
    //   );
    // }
    return SizedBox(
      width: 150,
      height: 250,
      child: Card(
        elevation: 8.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(_borderRadius),
          ),
        ),
        child: InkWell(
            onTap: () {
              Navigator.push(context,
                MaterialPageRoute(builder: (context) => DetailBarangScreen(imagesPath:imagesPath,nama:nama,harga:harga)
              ));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(_borderRadius),
                    topRight: Radius.circular(_borderRadius),
                  ),
                  child: Container(
                    color: Colors.red[800],
                    child: Image.network(
                      imagesPath,
                      width: 150,
                      height: 125,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    width: 150,
                    padding: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          nama,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red[800]),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(harga),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: RaisedButton(
                      color: Colors.red[800],
                      child: Text(
                        "Detail",
                        style: TextStyle(
                          color: Colors.white,
                      ),),
                      onPressed: () {
                        Navigator.push(context,
                          MaterialPageRoute(builder: (context) => DetailBarangScreen(imagesPath:imagesPath,nama:nama,harga:harga)
                        ));
                      },
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
}

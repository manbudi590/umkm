import 'package:flutter/material.dart';

class PopulerItemCard extends StatelessWidget {
  final String image;
  final String name;

  final double _cardRadius = 15;

  PopulerItemCard({
    @required this.image,
    @required this.name,
  })  : assert(image != null),
        assert(name != null);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Stack(
      overflow: Overflow.visible, children: <Widget>[
      Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(_cardRadius),
        ),
        child: ClipRRect(
          borderRadius: new BorderRadius.circular(_cardRadius),
          child: Image.network(
            image,
            width: width - 40,
            height: 180,
            fit: BoxFit.cover,
          ),
        ),
      ),
      Positioned(
        bottom: 20,
        left: 20,
        right: 20,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(_cardRadius),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  name,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text('Pelatihan Mobile adalah jshdbjdhjbsjdajddygdebhdbejhb', style: Theme.of(context).textTheme.subtitle),  
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  // child: SizedBox(
                    // width: double.infinity,
                    child: RaisedButton(
                      color: Colors.red[800],
                      child: Text(
                        "Detail",
                        style: TextStyle(
                          color: Colors.white,
                      ),),
                      onPressed: () {
                        // Navigator.push(context,
                        //   MaterialPageRoute(builder: (context) => DetailScreen()
                        // ));
                      },
                    ),
                  // ),
                )
                   
                  ],
                )
              ],
            ),
          ),
        ),
      )
    ]);
  }
}

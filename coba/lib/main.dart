
import 'package:coba/src/screen/Login/login.dart';
import 'package:flutter/material.dart';
import 'src/app.dart';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: '/',
      routes: <String, WidgetBuilder>{
        '/': (context) => Login(),
        '/login': (context) => App(),
      },
  ));
}
